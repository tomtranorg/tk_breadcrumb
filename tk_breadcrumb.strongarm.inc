<?php
/**
 * @file
 * tk_breadcrumb.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function tk_breadcrumb_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'path_breadcrumbs_internal_render';
  $strongarm->value = 0;
  $export['path_breadcrumbs_internal_render'] = $strongarm;

  return $export;
}
